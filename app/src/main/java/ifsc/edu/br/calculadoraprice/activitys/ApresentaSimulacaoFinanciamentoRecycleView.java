package ifsc.edu.br.calculadoraprice.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.NumberFormat;

import ifsc.edu.br.calculadoraprice.R;
import ifsc.edu.br.calculadoraprice.controler.Financiamento;
import ifsc.edu.br.calculadoraprice.model.Parcela;
import ifsc.edu.br.calculadoraprice.util.FinanciamentoAdapterRecycleView;

public class ApresentaSimulacaoFinanciamentoRecycleView extends AppCompatActivity {
    protected RecyclerView recyclerView;
    protected Financiamento financiamento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apresenta_simulacao_financiamento_recycle_view);

        this.recyclerView=findViewById(R.id.recyclerView);

        //Recuperando número de meses,valor emprestim e taxa de juros passados a esta activity por parametro na intent
        Intent intent=getIntent();
        int meses=intent.getExtras().getInt("meses");
        double valorEmprestimo=intent.getExtras().getDouble("valorEmprestimo");
        double txJuros=intent.getExtras().getDouble("txJuros");

        //Instanciando a classe financiando para gerar a simulação e assim os dados necessários para apreentção
        Financiamento f=  new Financiamento( meses,valorEmprestimo,txJuros);


        //Iniciando a apresentação com list View, primeiramente carregando e configurando o adaptador
        FinanciamentoAdapterRecycleView adapter = new FinanciamentoAdapterRecycleView( f.getParecelas());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration( new DividerItemDecoration(this, LinearLayout.VERTICAL));

        recyclerView.setAdapter(adapter);
        double soma = 0;
        for (Parcela p: f.getParecelas()){
            soma+=p.getValorPrestacao();
        }

        TextView tvTotalPago=(TextView)findViewById(R.id.total);
        String valorTotalCurrency =NumberFormat.getCurrencyInstance().format(soma);
        tvTotalPago.setText(valorTotalCurrency);



    }
}
