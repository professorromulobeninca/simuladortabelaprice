package ifsc.edu.br.calculadoraprice.util;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import ifsc.edu.br.calculadoraprice.model.Parcela;
import ifsc.edu.br.calculadoraprice.R;
public class FinanciamentoAdapterListView extends ArrayAdapter<Parcela>{
    private static  final String TAG = "Parcela";

    private Context mContext;
    private  int mResource;
    /**
     * Constructor
     * @param context
     * @param resource
     * @param objects
     */
    public FinanciamentoAdapterListView(@NonNull Context context, int resource, @NonNull ArrayList<Parcela> objects) {
        super(context, resource, objects);
        this.mContext=context;
        this.mResource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,  @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView= inflater.inflate(mResource, parent, false);

        TextView tvNumeroParcela= (TextView) convertView.findViewById(R.id.textViewNumeroParcela);
        TextView tvValorParcela= (TextView) convertView.findViewById(R.id.textViewValorParcela);
        TextView tvValorAmortizacao= (TextView) convertView.findViewById(R.id.textViewValorAmortizacao);
        TextView tvValorJuros= (TextView) convertView.findViewById(R.id.textViewValorJuros);
        TextView tvValorDividada= (TextView) convertView.findViewById(R.id.textViewValorDivida);

        NumberFormat nc = NumberFormat.getCurrencyInstance();
        tvNumeroParcela.setText(Integer.toString(getItem(position).getOrdemParcela()));
        tvValorParcela.setText(nc.format(getItem(position).getValorPrestacao()));
        tvValorAmortizacao.setText(nc.format(getItem(position).getAmortizacao()));
        tvValorJuros.setText(nc.format(getItem(position).getJuros()));
        tvValorDividada.setText(nc.format(getItem(position).getSaldoDevedor()));

        if ( getItem(position).getAmortizacao() <getItem(position).getJuros() ) {
            tvValorJuros.setTextColor(Color.RED);
        }else{
            tvValorJuros.setTextColor(Color.BLUE);
        }
        return  convertView;



    }
}
