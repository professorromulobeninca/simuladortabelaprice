package ifsc.edu.br.calculadoraprice.activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import java.text.NumberFormat;
import java.text.ParseException;
import ifsc.edu.br.calculadoraprice.R;
import ifsc.edu.br.calculadoraprice.controler.Financiamento;


public class MainActivity extends AppCompatActivity {
    SeekBar seekBarTxJuro;
    EditText editTextValorEmprestimo;
    EditText editTextValorTxJuro;
    EditText editTextMeses;

    //Propriedade NumberFormat, para formatação de dados porcentagem e monetários
    private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private static NumberFormat percentFormat  = NumberFormat.getPercentInstance();

    // Propriedades do container SharePreferences e Editor
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;

    //Propriedades da classe para armazenar os parametros informados para simulação.
    protected int meses;
    protected double valorEmprestimo;
    protected double txJuros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextValorEmprestimo= (EditText) findViewById(R.id.editTextValorEmprestimo) ;

        editTextValorTxJuro= (EditText) findViewById(R.id.editTextValorTxJuro);
        editTextValorEmprestimo.addTextChangedListener(amountEditTextWatcher);

        editTextMeses= (EditText) findViewById(R.id.editTextMeses);


        seekBarTxJuro= (SeekBar) findViewById(R.id.seekBarJuro);
        percentFormat.setMinimumFractionDigits(2);

        //Inicializando SharePreferences para armazenar os parametros da simulação
        this.preferences = getSharedPreferences("PREFERENCIAS", MODE_PRIVATE);
        this.editor=preferences.edit();



        seekBarTxJuro.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double valor= seekBar.getProgress();
                editTextValorTxJuro.setText((percentFormat.format(valor/1000).toString()));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


    }
    private final TextWatcher amountEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onStart() {
        super.onStart();

        //Recuperando paramtros salvos em SharePreferences
        editTextMeses.setText(Integer.toString(this.preferences.getInt("meses",10)));
        editTextValorEmprestimo.setText(currencyFormat.format(Double.parseDouble(this.preferences.getString("valorEmprestimo","1000.0"))));
        editTextValorTxJuro.setText(percentFormat.format(Double.parseDouble(this.preferences.getString("txJuros","0.1" ))));
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Salvando os parametros em SharePreferences
        this.editor.putInt("meses",this.meses);
        this.editor.putString("valorEmprestimo",Double.toString(this.valorEmprestimo));
        this.editor.putString("txJuros",Double.toString(this.txJuros));
        this.editor.commit();
    }

    public void simularRecycleView(View view) {
        Financiamento f= null;
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(2);

        this.meses=  Integer.parseInt( this.editTextMeses.getText().toString());
        this.valorEmprestimo=0.0;
        this.txJuros=0.1;

        try {
            valorEmprestimo= currencyFormat.parse(editTextValorEmprestimo.getText().toString()).doubleValue() ;
            txJuros= percentFormat.parse(editTextValorTxJuro.getText().toString()).doubleValue();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Carregando os valores a serem passados para proxíma activity em um bundle
        Bundle params = new Bundle();
        params.putInt("meses",meses);
        params.putDouble("valorEmprestimo", valorEmprestimo);
        params.putDouble("txJuros",txJuros);

        //Definindo uma nova intenção para abrir a simulação
        Intent intent= new Intent(this,ApresentaSimulacaoFinanciamentoRecycleView.class);

        //Carregnado o bundle construido na intent a ser transmitida na aplicação
        intent.putExtras(params);
        //Disparando a intent e inicilalizando uma nova ativity e ao mesmo tempo passando os parametros definidos.
        startActivity(intent);

    }


    public void simularListView(View view) {
        Financiamento f= null;
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(2);

        this.meses=  Integer.parseInt( this.editTextMeses.getText().toString());
        this.valorEmprestimo=0.0;
        this.txJuros=0.1;

        try {
            valorEmprestimo= currencyFormat.parse(editTextValorEmprestimo.getText().toString()).doubleValue() ;
            txJuros= percentFormat.parse(editTextValorTxJuro.getText().toString()).doubleValue();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Carregando os valores a serem passados para proxíma activity em um bundle
        Bundle params = new Bundle();
        params.putInt("meses",meses);
        params.putDouble("valorEmprestimo", valorEmprestimo);
        params.putDouble("txJuros",txJuros);

        //Definindo uma nova intenção para abrir a simulação
        Intent intent= new Intent(this,ApresentaSimulacaoFinanciamentoListView.class);

        //Carregnado o bundle construido na intent a ser transmitida na aplicação
        intent.putExtras(params);
        //Disparando a intent e inicilalizando uma nova ativity e ao mesmo tempo passando os parametros definidos.
        startActivity(intent);

    }




}
