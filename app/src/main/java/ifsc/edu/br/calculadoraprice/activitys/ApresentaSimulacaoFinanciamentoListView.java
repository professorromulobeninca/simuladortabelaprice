package ifsc.edu.br.calculadoraprice.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.text.NumberFormat;

import ifsc.edu.br.calculadoraprice.R;
import ifsc.edu.br.calculadoraprice.controler.Financiamento;
import ifsc.edu.br.calculadoraprice.model.Parcela;
import ifsc.edu.br.calculadoraprice.util.FinanciamentoAdapterListView;

public class ApresentaSimulacaoFinanciamentoListView extends AppCompatActivity {
    protected ListView listView;
    protected Financiamento financiamento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apresenta_simulacao_financiamento_list_view);

        this.listView=findViewById(R.id.listView);

        Intent intent=getIntent();
        int meses=intent.getExtras().getInt("meses");
        double valorEmprestimo=intent.getExtras().getDouble("valorEmprestimo");
        double txJuros=intent.getExtras().getDouble("txJuros");

        Financiamento f=  new Financiamento( meses,valorEmprestimo,txJuros);

        FinanciamentoAdapterListView adapter = new FinanciamentoAdapterListView(this,
                R.layout.template_list_parcela_financiamento,
                f.getParecelas());

        listView.setAdapter(adapter);
        double soma = 0;
        for (Parcela p: f.getParecelas()){
            soma+=p.getValorPrestacao();
        }

        TextView tvTotalPago=(TextView)findViewById(R.id.total);
        String valorTotalCurrency =NumberFormat.getCurrencyInstance().format(soma);
        tvTotalPago.setText(valorTotalCurrency);



    }
}
