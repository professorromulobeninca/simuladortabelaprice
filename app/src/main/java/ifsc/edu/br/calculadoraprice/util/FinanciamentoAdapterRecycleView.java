package ifsc.edu.br.calculadoraprice.util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.text.NumberFormat;
import java.util.ArrayList;

import ifsc.edu.br.calculadoraprice.model.Parcela;
import ifsc.edu.br.calculadoraprice.R;


public class FinanciamentoAdapterRecycleView extends RecyclerView.Adapter<FinanciamentoAdapterRecycleView.MyViewHolder> {
    ArrayList<Parcela> listaParcelas;
    //Metodo contrutor do adapter que recebe a lista de parcelas do financiamento.
    public FinanciamentoAdapterRecycleView(ArrayList <Parcela> listaParcelas) {
        this.listaParcelas=listaParcelas;
    }


    //Metodo que carrega o template  do layout XML  no item de lista
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_list_parcela_financiamento, parent, false);
        return new MyViewHolder(itemLista);
    }

    //Metodo que faz a associação dos dados com os layouts
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Parcela p = listaParcelas.get(position);
        NumberFormat nc = NumberFormat.getCurrencyInstance();
        holder.tvNumeroParcela.setText(Integer.toString(p.getOrdemParcela()));
        holder.tvValorAmortizacao.setText(nc.format(p.getAmortizacao()));
        holder.tvValorDividada.setText(nc.format(p.getSaldoDevedor()));
        holder.tvValorParcela.setText(nc.format(p.getValorPrestacao()));
        holder.tvValorJuros.setText(nc.format(p.getJuros()));


    }
    //Metodo que defineo núemro de elementos que devem ser renderizados
    @Override
    public int getItemCount() {
        return listaParcelas.size();
    }
//*******************************
//Declaração da innerClass  de FinancimentoAdapter responsavel por associar os compoenentes do layout, utilizando seu contrutor que recebe os itens da view
public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView tvNumeroParcela;
    TextView tvValorParcela;
    TextView tvValorAmortizacao;
    TextView tvValorJuros;
    TextView tvValorDividada;

    public MyViewHolder(View itemView) {
        super(itemView);
        tvNumeroParcela = (TextView) itemView.findViewById(R.id.textViewNumeroParcela);
        tvValorParcela = (TextView) itemView.findViewById(R.id.textViewValorParcela);
        tvValorAmortizacao = (TextView) itemView.findViewById(R.id.textViewValorAmortizacao);
        tvValorJuros = (TextView) itemView.findViewById(R.id.textViewValorJuros);
        tvValorDividada = (TextView) itemView.findViewById(R.id.textViewValorDivida);
    }
}

}